# corpus-students-essays

This project reunites the annotated corpus and the annotation guidelines for annotating syntactic errors in students essays.

### Content

- File `corpus-public.ods` contains the original corpus with human grades in the five Enem competences and the global grades
- File `corpus-anotado-fase1.ods` contains 8,654 sentences in the train set and 1,998 sentences in the test set, all of them classified in 'D' (contains syntactic error) and 'N' (contains no syntactic error)
- File `corpus-anotado-fase2.tsv` contains 2,500 sentences automatically annotated with UDPipe (CONLL-U format) and manually annotated folowing the syntactic errors classification taxonomy described in the guidelines
- File `diretriz-anotacao_v1_25-01-19.pdf` contains the guidelines for the annotation of the corpus

All files, essays, sentences and guidelines are in Brazilian Portuguese.
